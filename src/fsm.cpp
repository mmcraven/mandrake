#include "fsm.h"

#include "Wire.h"

#include "enu.h"

void lamp_fsm::transition(lamp_event event)
{
  if(event == lamp_event::kTime) elapsed_seconds+= 1;
  else if(event == lamp_event::kSwitchedOn && state == lamp_state::kOn) return;
  else if(event == lamp_event::kSwitchedOff && (state == lamp_state::kSwitchedOff 
    || state == lamp_state::kOff)) return; 

  switch(state)
  {
    
  case lamp_state::kConfig:
    state = lamp_state::kOn;
    elapsed_seconds = 0;
    break;
      
  case lamp_state::kOn:
    if(event == lamp_event::kSwitchedOff) {
      state = lamp_state::kSwitchedOff;
      elapsed_seconds = 0;
    }
    else if(elapsed_seconds >= time_on) {
      state = lamp_state::kOff;
      elapsed_seconds = 0;
    }
    break;
    
  case lamp_state::kOff:
    if(event == lamp_event::kSwitchedOff) {
      elapsed_seconds = 0;
      state = lamp_state::kSwitchedOff;
    }
    else if(elapsed_seconds >= time_on) {
      state = lamp_state::kOn;
      elapsed_seconds = 0;
    }
    break;
    
  case lamp_state::kSwitchedOff:
    if(event == lamp_event::kSwitchedOn) {
      elapsed_seconds = 0;
      state = lamp_state::kOn;
    }
    break;
    
  }
}

void blinker_fsm::transition()
{
  switch(state)
  {
  case blink_state::kOn:
    state = blink_state::kOff;
    digitalWrite(LED_BUILTIN, HIGH); 
    break;
  case blink_state::kOff:
    state = blink_state::kOn;
    digitalWrite(LED_BUILTIN, LOW);
    break;
  }
}
