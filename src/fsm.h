#pragma once



#include <Adafruit_NeoPixel.h>

enum class lamp_event
{
  kTime,
  kSwitchedOn,
  kSwitchedOff
};

enum class lamp_state
{
  kConfig,
  kOn,
  kOff,
  kSwitchedOff
};


struct lamp_fsm
{
  lamp_state state = {lamp_state::kConfig};
  uint32_t elapsed_seconds = {0};

  void transition(lamp_event event = lamp_event::kTime);
};

enum class blink_state
{
  kOn,
  kOff
};

struct blinker_fsm
{
  blink_state state = blink_state::kOff;
  
  void transition();
  
};
