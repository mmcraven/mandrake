/*
 * The following program is used to run an automatic on/off grow light.
 * This code is in the public domain.
 */
#include <stdint.h>

#include <RTClib.h>
#include <Adafruit_NeoPixel.h>

#include "fsm.h"
#include "enu.h"


// NEOPIXEL BEST PRACTICES for most reliable operation:
// - Add 1000 uF CAPACITOR between NeoPixel strip's + and - connections.
// - MINIMIZE WIRING LENGTH between microcontroller board and first pixel.
// - NeoPixel strip's DATA-IN should pass through a 300-500 OHM RESISTOR.
// - AVOID connecting NeoPixels on a LIVE CIRCUIT. If you must, ALWAYS
//   connect GROUND (-) first, then +, then data.
// - When using a 3.3V microcontroller with a 5V-powered NeoPixel strip,
//   a LOGIC-LEVEL CONVERTER on the data line is STRONGLY RECOMMENDED.
// (Skipping these may work OK on your workbench but can fail in the field)
Adafruit_NeoPixel strip(32, NP_PIN, NEO_GRB + NEO_KHZ800);

/*
 * Clock configuration
 */
RTC_PCF8523 rtc;

#ifdef MOISTURE_BUTTON
#include <Adafruit_seesaw.h>
/*
 * Soil sensor configuration.
 */
uint16_t min_cap, max_cap;
Adafruit_seesaw ss;
#endif

#ifdef USE_LCD
#include <Wire.h>
#include <hd44780.h>
#include <hd44780ioClass/hd44780_I2Cexp.h>
const int LCD_COLS = 16;
const int LCD_ROWS = 2;
hd44780_I2Cexp lcd; // declare lcd object: auto locate & auto config expander chip
#endif

void init_rtc()
{
  if (!rtc.begin()) {
    Serial.println("Couldn't find RTC");
    Serial.flush();
    abort();
  }
}

lamp_fsm lamp;
blinker_fsm blinker;
void step()
{
  lamp.transition();
  blinker.transition();
}

// Fill strip pixels with a color.
void set_lamp(Adafruit_NeoPixel&, uint32_t color)
{
  for (int i = 0; i < strip.numPixels(); i++)
  {
    strip.setPixelColor(i, color); //  Set pixel's color (in RAM)
  }
  strip.show(); //  Update strip to match
}

void setup()
{


  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(NP_PIN, OUTPUT);
  // Set the pin attached to PCF8523 INT to be an input with pullup to HIGH.
  pinMode(TIMER_PIN, INPUT_PULLUP);
  
  #ifdef LIGHT_OFF
  pinMode(LIGHT_OFF, INPUT);
  pinMode(LIGHT_ON, INPUT);
  #endif
  
  init_rtc();
  
  strip.begin(); // INITIALIZE NeoPixel strip object (REQUIRED)
  strip.show();  // Turn OFF all pixels

  
  digitalWrite(NP_PIN, LOW);
  rtc.enableSecondTimer();  // 10 seconds
  attachInterrupt(digitalPinToInterrupt(TIMER_PIN), step, FALLING);

  #ifdef MOISTURE_BUTTON
  pinMode(MOISTURE_BUTTON, INPUT_PULLUP);
  // Init soil sensor
  if (!ss.begin(0x36)) {
    Serial.println("ERROR! seesaw not found");
    while(1);
  }
  min_cap = max_cap = ss.touchRead(0);
  #endif

  #ifdef USE_LCD
  auto status = lcd.begin(LCD_COLS, LCD_ROWS);
	if(status) hd44780::fatalError(status); // hd44780 has a fatalError() routine that blinks an led 
  // Backlight enabled by .begin(), display message.
	lcd.print("Elapsed Time:");
  #endif

}

bool lamp_off=0, lamp_on=0;
void loop()
{
  #ifdef LIGHT_OFF
  // The toggle that is off may still read "1"
  lamp_off = digitalRead(LIGHT_OFF);
  lamp_on = digitalRead(LIGHT_ON);
  if(lamp_off ==lamp_on){}
  else if(lamp_off) lamp.transition(lamp_event::kSwitchedOff);
  else if(lamp_on) lamp.transition(lamp_event::kSwitchedOn);
  Serial.print(lamp_off);
  Serial.print(" ");
  Serial.print(lamp_on);
  #endif

  
  uint32_t color = 0;
  #ifdef MOISTURE_BUTTON
  int reset_moisture = digitalRead(MOISTURE_BUTTON);
  uint16_t capread = ss.touchRead(0);
  if(!reset_moisture) {
    min_cap = max_cap = ss.touchRead(0);
  }
  else {
    max_cap = max(max_cap, capread);
    min_cap = min(min_cap, capread);
  }

  auto dif = max_cap-min_cap; 
  auto res_cap = (capread-min_cap);
  
  if(lamp.state == lamp_state::kOn) {
    if((dif< 200) || (dif/2 < res_cap)) {
      color = strip.Color(color_on.red, color_on.green, color_on.blue);
    }
    else {
      auto scale = 2*(dif/2 - res_cap)/dif;
      color = strip.Color(color_on.red, (uint8_t) scale * color_on.green, color_on.blue);
    }
  }
  #else
  if(lamp.state == lamp_state::kOn) {
    color = strip.Color(color_on.red, color_on.green, color_on.blue);
  }
  #endif
  else if(lamp.state == lamp_state::kOff || lamp.state == lamp_state::kSwitchedOff) {
    color = strip.Color(color_off.red, color_off.green, color_off.blue);  
  }
  set_lamp(strip, color);

  #ifdef USE_LCD
  lcd.setCursor(0, 1);
  lcd.print((uint32_t) lamp.elapsed_seconds);
  lcd.print(" ");
  lcd.print((uint32_t) time_on);
  lcd.print("                ");
  #endif

  #ifdef LED_PIN
  if(lamp.state != lamp_state::kSwitchedOff) digitalWrite(LED_PIN, blinker.state != blink_state::kOn? HIGH : LOW);
  else digitalWrite(LED_PIN, LOW);
  #endif
  
  delay(100);
}
