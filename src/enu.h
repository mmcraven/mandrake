#pragma once
#include <stdint.h>

// Number of seconds between transitions
static const uint32_t time_setup = 3;
static const uint32_t time_on = 60LLU*60LLU*12LLU;

struct colors
{
  colors(){}
  colors(uint8_t r, uint8_t g, uint8_t b): red(r), green(g), blue(b)
  {
  }
  uint8_t red = {0}, green = {0}, blue = {0};
} static color_on(255,255,128), color_off;
